.data
output1: .asciiz "Penjumlahan digit-digit "
output2: .asciiz " adalah "

.text
.globl main
main: 
	li $t1, 27042002
	move $t3, $t1
	move $t6, $t1		#print in outputy
	move $t0, $zero		#sum of digit
	move $t2, $zero		#counter
	addi $t5, $zero,10
	
BanyakDigit:
	div $t3, $t5
	mfhi $t4		#sisa bagi
	mflo $t3		#hasil bagi
	addi $t2, $t2, 1
	beq $t3, $zero , JumlahinDigit
	j BanyakDigit

JumlahinDigit:
	div $t1,$t5
	mfhi $t4
	mflo $t1
	add $t0, $t0, $t4
	beq $t1, $zero, Exit
	j JumlahinDigit
	
Exit:
	li $v0,4 		#instruction to print string output1
	la $a0,output1 		#move string output to argumen
	syscall 
	
	li $v0,1 		#instruction to print sum digit
	move $a0,$t6	 	#move integer to argumen
	syscall
	
	li $v0,4 		#instruction to print string output2
	la $a0,output2 		#move string output to argumen
	syscall 
	
	li $v0,1 		#instruction to print sum digit
	move $a0,$t0	 	#move integer to argumen
	syscall
	
	li $v0,10		#exit command
	syscall
