.data
fibonacci: .word   0 : 12	# "array" of 12 words to contain fib values
size: .word  12             	# size of "array" 
space:.asciiz  " "          	# space to insert between numbers
#head: .asciiz  "The Fibonacci numbers are:\n"

.text
      	la   $t0, fibonacci	# load address of array
      	la   $s0, size        	# load address of size variable
      	lw   $s0, 0($s0)      	# load array size
      	li   $a1, 1           	# 1 is first and second Fib. number
     
      	sw   $a1, 0($t0)      	# F[0] = 1
      	sw   $a1, 4($t0)      	# F[1] = F[0] = 1
      	addi $t1, $s0, -2     	# Counter for loop, will execute (size-2) times
loop: 
	lw   $t3, 0($t0)      	# Get value from array F[n] 
      	lw   $t4, 4($t0)      	# Get value from array F[n+1]
      	add  $a1, $t3, $t4    	# $a1 = F[n] + F[n+1]
      	sw   $a1, 8($t0)      	# Store F[n+2] = F[n] + F[n+1] in array
      	addi $t0, $t0, 4      	# increment address of Fib. number source
      	addi $t1, $t1, -1     	# decrement loop counter
      	bgtz $t1, loop        	# repeat if not finished yet.
      	
      	la   $a0, fibonacci	# first argument for print (array)
      	move  $s1, $s0		# second argument for print (size)
      	jal  print            
      	
      	li   $v0, 10         
      	syscall               
		
#########  routine to print the numbers on one line. 

print:
	move  $t0, $a0  	# starting address of array
      	move  $t1, $s1  	# initialize loop counter to array size
      	#la   $a0, head        	# load address of print heading
      	#li   $v0, 4           	
      	#syscall               	
out:  
	lw   $a0, 0($t0)      
      	li   $v0, 1           # specify Print Integer service
      	syscall               # print fibonacci number
      	
      	la   $a0, space       
      	li   $v0, 4           