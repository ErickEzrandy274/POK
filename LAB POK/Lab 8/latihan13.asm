;====================================================================
; Processor		: ATmega8515
; Compiler		: AVRASM
;====================================================================

;====================================================================
; DEFINITIONS
;====================================================================

.include "m8515def.inc"

;====================================================================
; RESET and INTERRUPT VECTORS
;====================================================================

.org $00 	; JUMP to MAIN to initialze
	rjmp MAIN
.org $01	; When Button0 pressed, jump to ext_int0
	rjmp ext_int0
.org $07	; When Timer0 overflows, jump to ISR_TOV0
	rjmp ISR_TOV0

;====================================================================
; CODE SEGMENT
;====================================================================

MAIN:
	;init Stack Pointer	
	ldi r20, 1 		; buat masuk ke dalam
	ldi r24, 1		; buat pengurangan
	ldi r16, low(RAMEND)
	out SPL, r16
	ldi r16, high(RAMEND)
	out SPH, r16

	ldi ZH, HIGH(2*SOMETHING)	//ambil address yang high and save into ZH(r31)
	ldi ZL, LOW(2*SOMETHING)	//ambil address yang low and save into ZL(r30)

; Setup LED PORT
SET_LED:
	ser r16			; Load $FF to temp		
	out DDRB, r16	; Set PORTB to output	

; Setup Overflow Timer0
SET_TIMER:
	; Timer speed = clock/1024 (set CS02 and CS00 in TCCR0)
	ldi r16, (1<<CS02) | (1<<CS00)
	out TCCR0, r16

	; Execute an internal interrupt when Timer0 overflows
	ldi r16, (1<<TOV0)
	out TIFR, r16

	; Set Timer0 overflow as the timer
	ldi r16, (1<<TOIE0)
	out TIMSK, r16

	; Set global interrupt flag
	sei

; While waiting for interrupt, loop infinitely
FOREVER:
	rjmp FOREVER

; Program executed on button press
ext_int0:
	reti

; Program executed on timer overflow
ISR_TOV0:
	push r16
	in r16, SREG
	push r16

	cpi r20, 0			; r20 sebagai penanda dia masuk atau keluar
	breq keluar
	
	lpm r21, Z+			; ambil value yg ada di db
	out PORTB, r21		; write to PORTB

	cpi r21, $89		; kalau nilainya == $89 akan reset
	breq reset

	pop r16
	out SREG, r16
	pop r16
	reti

	keluar:
		
		sbiw ZL, 1		; buat ngambil value sebelumnya nanti
		lpm r21, Z
		out PORTB, r21
		

		cpi r21, $11	; kalo valueny sama maka akan diset untuk masuk ke dalam
		breq seti
		reti

	reset:
		sub r20, r24	; pengurangan untuk keluar nantinya
		sbiw ZL, 1
		ret

	seti:
		add r20, r24	; penjumlahan untuk masuk ke dalam nantinya
		adiw ZL, 1
		ret


SOMETHING:
.db $11, $22, $44, $88, $89
