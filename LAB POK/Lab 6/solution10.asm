.include "m8515def.inc"


.def sem =r18
.def temp=r16


; Solusi ini menggunakan PORT B
; Silahkan disesuaikan PORT masing-masing

INIT:		
	ldi	temp,low(RAMEND) 	
	out	SPL,temp	 		
	ldi	temp,high(RAMEND)
	out	SPH,temp


Led_Opening:
	ldi sem, 0b00011000
	out PORTB, sem	; output to PORT B
	rcall DELAY

	ldi sem, 0b00100100
	out PORTB, sem
	rcall DELAY

	ldi sem, 0b01000010
	out PORTB, sem
	rcall DELAY

	ldi sem, 0b10000001
	out PORTB, sem
	rcall DELAY



Led_NPM:
	ldi zh, HIGH(data_npm*2)
	ldi zl, LOW(data_npm*2)

	ldi temp, 10
	
	loop_npm:
		tst temp
		breq led_data
		dec temp
		lpm
		adiw zl, 1
		mov sem, r0
		out PORTB, sem
		rcall DELAY
		rjmp loop_npm


Led_data:
	ldi xh, high($81)
	ldi xl, low($81)
	ld sem, x+
	out portB, sem
	rcall delay

Led_Close:
	ldi sem, 0b10000001
	out PORTB, sem
	rcall DELAY

	ldi sem, 0b01000010
	out PORTB, sem
	rcall DELAY

	ldi sem, 0b00100100
	out PORTB, sem
	rcall DELAY

	ldi sem, 0b00011000
	out PORTB, sem	; output to PORT B
	rcall DELAY

forever:
	rjmp forever


; Delay 400 000 cycles
; 100ms at 4 MHz

DELAY:
    ldi  r18, 3
    ldi  r19, 8
    ldi  r20, 120
L1: dec  r20
    brne L1
    dec  r19
    brne L1
    dec  r18
    brne L1
	ret


data_npm:
	.db 1,9,0,6,2,8,5,5,5,4
	.db 0,0 

