.include "m8515def.inc"
.def rone = r1
.def input = r16
.def rslt = r17
.def less1 = r4
.def less2 = r18
.def temp = r19
.def reg2 = r20 
.equ BLOCK1=$81

RESET:
    ldi temp,low(RAMEND)
    out SPL,temp ;init Stack Pointer
    ldi temp,high(RAMEND)
    out SPH,temp
    
main:
	ldi YH, high(BLOCK1)
	ldi YL, low(BLOCK1)

    ldi ZH, high(DATA*2)
    ldi ZL, low(DATA*2)
    lpm input, Z			    //get the value
	
    ldi temp, 3					//karena base casenya kurang dari 3
    mov rone, temp

    rcall POKnacci
    st Y,rslt                   //save result to $81

forever:
    rjmp forever

POKnacci:
    push input
    push less1
    push less2
    push temp
    
    cp input, rone
    brlt one            		// jika input < 3

    mov less1, input
    dec less1                   //f(n-1)
    mov less2, input
    subi less2, 2               //f(n-2)

    mov input, less1
    rcall POKnacci   
	lsr rslt					//dibagi 2
    mov temp, rslt

    mov input, less2
    rcall POKnacci
	lsl rslt					//dikali 2	
    add rslt, temp
    rjmp done

one:
    ldi rslt, 1                	//case if p(1) or p(2)

done:
    pop temp
    pop less2
    pop less1
    pop input
    ret

DATA:
    .db 5, 0 ; input
