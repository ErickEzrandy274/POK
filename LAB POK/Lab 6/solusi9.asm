.include "m8515def.inc"


; program variables
.equ varA = $60
.equ varB = $61
.equ varQ = $62
.equ varR = $63
.equ input_var = $80
.equ output_var = $81


; registers definition

.def temp=r16 ;temporary storage variable
.def temp2=r17

.def quotient=r4
.def remainder=r5

RESET:
    ldi temp,low(RAMEND)
    out	SPL,temp	;init Stack Pointer		
    ldi	temp,high(RAMEND)
    out	SPH,temp


data2sram: ; pindah data dari DATA ke SRAM dengan alamat input_var
    
	ldi ZH, HIGH(DATA*2)
	ldi ZL, LOW(DATA*2)

	lpm
	ldi r27, high(input_var)
	ldi r26, low(input_var)

	st x+, r0
	
	adiw ZL, 1

main:
	rcall POKnacci
	

forever:
	rjmp forever


; input: input_var
; output: output_var
; using registers: r2, r3, temp, temp2, y pointer
POKnacci:
	ldi r29, high(input_var)
	ldi r28, low(input_var)
	ld r2, y
	push r2

	base_case:
		ldi temp, 3
		cp r2, temp
		brlt end_case

	rec_case:
		mov r3, r2
		dec r3
		ldi r29, high(input_var)
		ldi r28, low(input_var)
		st y, r3
		rcall POKnacci
		ldi r29, high(output_var)
		ldi r28, low(output_var)
		ld temp, y
		
		ldi r29, high(varA)
		ldi r28, low(varA)
		st y+, temp
		ldi temp, 2
		st y+, temp

		mov r3, r2
		push r3
		rcall divide
		pop r3

		ldi r29, high(varQ)
		ldi r28, low(varQ)
		ld temp, y
		
		dec r3
		ldi r29, high(input_var)
		ldi r28, low(input_var)
		st y, r3

		push temp
		rcall POKnacci
		

		ldi r29, high(output_var)
		ldi r28, low(output_var)
		ld temp2, y

		
		ldi temp, 2
		mul temp2, temp
		mov temp2, r0
		pop temp

		add temp, temp2
		
		ldi r29, high(output_var)
		ldi r28, low(output_var)
		st y, temp
		rjmp out_rec
		
	end_case:
	ldi r29, high(output_var)
	ldi r28, low(output_var)
	ldi temp, 1
	st y, temp
	out_rec:
		pop r2
		ret
	



; input: varA, $varB
; output: 
;	varQ = varA/varB
;	varR = varA mod varB
; using registers: r2, r3, quotient, remainder, x pointer
divide:
	ldi r27, high(varA)
	ldi r26, low(varA)

	ldi temp, 0
	mov quotient, temp

	ld r2, x+
	ld r3, x+

	div_loop:
		cp r2, r3
		brlt div_loop_end
		sub r2, r3
		inc quotient
		rjmp div_loop
	
	div_loop_end:
		mov remainder, r2
		ldi r27, high(varQ)
		ldi r26, low(varQ)
		st x+, quotient
		st x+, remainder
	ret


DATA:
	.db 7, 0
