.include "m8515def.inc"
.def result = r1

main:
	ldi ZH, HIGH(2*SOMETHING)
	ldi ZL, LOW(2*SOMETHING)
	lpm r2, Z+					//get the first value
	lpm r3, Z					//get the second value
	mov r4, r2
    mov r5, r3

valid:
    cp r4, r5
    brlt loop1
    breq stop					//branch happen if we get lcm(r4,r5)
    add r5, r3					//r5 = r5 + r3
    rjmp valid

loop1:
    add r4, r2					//r4 = r4 + r2
    rjmp valid

stop:
    mov result, r5				//move value from r5 to result(r1)

forever :
    rjmp forever
	
SOMETHING:
.db 2, 3
.db 0, 0