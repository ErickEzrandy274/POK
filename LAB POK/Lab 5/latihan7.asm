.include "m8515def.inc"
.def result = r2
main:
	ldi ZH, HIGH(2*SOMETHING)	//ambil address yang high and save into ZH(r31)
	ldi ZL, LOW(2*SOMETHING)	//ambil address yang low and save into ZL(r30)

loop:
	lpm							//ambil value di address yang ada di register Z dan dimasukkan ke r0
	tst r0						//test r0
	breq stop					//branch if r0 == 0
	mov r16, r0					//move value from r0 to r16

funct1:
	cpi r16, 3					//compare r16 to the value 3
	brlt funct2					//branch if r16 < 3 
	subi r16, 3					//r16 = r16 - 3
	rjmp funct1					//jump to funct1

funct2:
	add r1, r16					//r1 = r1 + r16
	adiw ZL, 1					//ZL = ZL + 1
	rjmp loop					//jump to loop

stop:
	mov result, R1				//move value from R1 to result(R2)
								//nilai dari result adalah 0x07

forever:
	rjmp forever				//jump to forever

SOMETHING:
.db 2, 11, 7, 8
.db 0, 0
