.data
permenA: .word 200
permenB: .word 300
permenC: .word 500
batasAwal: .word 0
batasAkhir: .word 100
hargaAwal: .word 1
hargapermenA: .asciiz "Harga permen A: "
hargapermenB: .asciiz "\nHarga permen B: "
hargapermenC: .asciiz "\nHarga permen C: "
diskonPermen: .asciiz "\nDiskon permen: "
output1: .asciiz "\nTotal Harga setelah didiskon: "
output2: .asciiz "\nTidak mungkin harga atau diskon suatu permen merupakan nol atau negatif !!! "
output3: .asciiz "\nTidak mungkin diskon suatu permen lebih dari 100% !!! "

.text
.globl main
main:	
	lw $s1,permenA		#masukkin jumlah permen A yang dibeli
	lw $s2,permenB		#masukkin jumlah permen B yang dibeli
	lw $s3,permenC		#masukkin jumlah permen C yang dibeli
	lw $s4,batasAwal	#masukkin batas awal diskon
	lw $s5,batasAkhir	#masukkin batas akhir diskon
	lw $s7,hargaAwal	#inisialisasi $s7 dengan 1 supaya harga permen lebih dari 0
	
	
	li $v0,4 		#instruction to print string hargapermenA
	la $a0,hargapermenA 	#move string hargapermenA to argumen
	syscall 		#execute
	
	li $v0,5 		#read integer untuk harga permen A from user
	syscall 
	
	move $t1,$v0		#move integer input to $t1
	
	li $v0,4 		#instruction to print string hargapermenB
	la $a0,hargapermenB 	#move string hargapermenB to argumen
	syscall 		#execute
	
	li $v0,5 		#read integer untuk harga permen B from user
	syscall 
	
	move $t2,$v0		#move integer input to $t2
	
	li $v0,4 		#instruction to print string hargapermenC
	la $a0,hargapermenC 	#move string hargapermenC to argumen
	syscall 		#execute
	
	li $v0,5 		#read integer untuk harga permen C from user
	syscall 
	
	move $t3,$v0		#move integer input to $t3
	
	li $v0,4 		#instruction to print string diskonPermen
	la $a0,diskonPermen	#move string diskonPermento argumen
	syscall 		#execute
	
	li $v0,5 		#read integer untuk diskon permen from user
	syscall 
	
	move $t4,$v0		#move integer diskon permen to $t4
	
	blt $t1,$s7,Akhir	#jika harga permen lebih kecil dari 0
	blt $t2,$s7,Akhir	#maka akan langung diarahkan ke label Akhir
	blt $t3,$s7,Akhir
	
	blt $t4,$s4,Akhir	#cek diskonnya > 100 atau < 0
	bgt $t4,$s5,Akhir1
	
	sub $t4,$s5,$t4		#harga total setelah dipotong diskon
	
	mul $t6,$s1,$t1		#mengalikan harga permen dengan jumlah permen yang dibeli
	mul $t7,$s2,$t2
	
	add $t6,$t6,$t7		#total harga = A + B
	mul $t7,$s3,$t3
	add $t6,$t6,$t7		#total harga += C
	
	mul $t6,$t6,$t4
	div $t6,$t6,$s5
	
	li $v0,4 		#instruction to print string output1
	la $a0,output1 		#move string output1 to argumen
	syscall
	
	li $v0,1 		#instruction to print total harga
	add $a0,$t6,$zero 	#move integer to argumen
	syscall
	j Exit
	
Akhir :
	li $v0,4 		#instruction to print string output2
	la $a0,output2 		#move string output2 to argumen
	syscall 		#execute
	j Exit

Akhir1 :
	li $v0,4 		#instruction to print string output3
	la $a0,output3 		#move string output3 to argumen
	syscall 		#execute	
	j Exit	

Exit: 
	li $v0,10		#exit command
	syscall		
