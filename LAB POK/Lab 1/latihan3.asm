.data
input: .asciiz "Jumlah teman yang kamu bawa: "
output1: .asciiz "\nTiap orang akan mendapatkan "
permen: .asciiz " permen"
output2: .asciiz "\nInput harus berada pada rentang 0-999"
awalan: .word 0
akhiran: .word 999
diriKita: .word 1
jumPermen: .word 1000

.text
.globl main
main:
	lw $s0,awalan		#buat handle kalo user masukin input negatif
	lw $s1,akhiran		#buat handle kalo user masukin input > 999
	lw $s2,diriKita		#buat di + ama temen yang kite bawa 
	lw $s3,jumPermen	
	
	li $v0,4 		#instruction to print string input
	la $a0,input 		#move string input to argumen
	syscall 		#execute
	
	li $v0,5 		#read integer from user
	syscall 
	
	move $t0,$v0		#move integer input to $t0
	
	bgt $t0,$s1,Akhir	#kalau input > 999 langsung keluar output
	blt $t0,$s0,Akhir	#kalau input < 0 langsung keluar output 
	
	addi $t0,$t0,1 		#temen yang kite bawa di +1
	div $s3,$t0		#dapet berapa tiap orang
	mflo $t1
	
	li $v0,4 		#instruction to print string output1
	la $a0,output1 		#move string output1 to argumen
	syscall
	
	li $v0,1 		#instruction to print integer
	add $a0,$t1,$zero 	#move integer to argumen
	syscall
	
	li $v0,4 		#instruction to print string permen
	la $a0,permen 		#move string permen to argumen
	syscall
	j Exit			#biar ga masuk ke label Akhir
	
Akhir :
	li $v0,4 		#instruction to print string output2
	la $a0,output2 		#move string output2 to argumen
	syscall 		#execute

Exit: 
	li $v0,10		#exit command
	syscall		