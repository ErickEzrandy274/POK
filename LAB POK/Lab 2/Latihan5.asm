.data
myId: .word 2, 0, 0, 6, 5, 9, 5, 8, 9, 2

inputId: .asciiz "Apakah anda ingin memasukkan id baru? (0/1) "
input1: .asciiz "Masukkan Input berapa digit: "
input2: .asciiz "Masukkan Input: "
output1: .asciiz "Anda bukan children"

.text 
.globl main

main:	
	#instruction to print string inputId
	li $v0,4			
	la $a0,inputId			
	syscall
	
	#read integer from user	and  move inputId to $t0
	li $v0,5			
	syscall
	move $t0,$v0			#mindahin id baru user ke $t0
	
	#handle input user 1 atau 0
	beq $t0,1,Validate		#jika $t0 bernilai 1 maka akan masuk ke Validate
	addi $t0,$zero, 10		
	
	j Label1			#lompat ke Label1
	
Validate:
	#instruction to print string input1
	li $v0,4			
	la $a0,input1		
	syscall
	
	#read number integer from user	and  move inputId to $t0
	li $v0,5			
	syscall
	move $t0,$v0		
	
	#handle jika input n < 2 atau n > 10
	blt $t0,2,Output1		
	bgt $t0,10,Output1		
	
	#instruction to print string input2
	li $v0,4			
	la $a0,input2		
	syscall
	
	#read number integer from user	and  move inputId to $t1
	li $v0,5			
	syscall
	move $t1,$v0		
	
	move $t3,$t0			#move n digit ke $t3
	la $t4,myId			#put address of myId
	addi $t7,$zero,10		#buat ambil modulonya pas dibagi 10
	
	j StoreDigit		

StoreDigit:
	#loop untuk nyimpen setiap digit ke memory
	beq $t3,0,Label1
	
	div $t1,$t7			#input baru dibagi dengan 10
	mfhi $t8			#nyimpen sisa pembagian ke $t8
	mflo $t1			#nyimpen hasil pembagian ke $t1			
	sw $t8,0($t4)			#nyimpen digit satuan di address $t4
	
	addi $t4,$t4,4			#di + 4 pada address myId
	addi $t3,$t3,-1			#ngurangin loop counter
	j StoreDigit		
	
Label1:
	add $t3, $t0, $zero		#loop counter dengan nilai awal jumlah digit
	addi $t7, $zero,4		
	la $t4,myId			#nyimpen address myId ke $t4
	
	mult $t0,$t7			
	mflo $t8			#nyimpen hasil kali ke $t8
	
	addi $t8,$t8,-4
	add $t8,$t4,$t8
	
	j Penjumlahan			
	
Penjumlahan:
	# menjumlahkan digit pertama dengan terkahir, digit kedua dengan digit kedua sebelum akhir, dst
	beq $t3,0,Exit			#ketika counter == 0, loop berakhir dan lompat ke exit
	lw $t1,0($t4)			#ambil digit ke-i dan disimpen di $t1
	lw $t2,0($t8)			#ambil digit ke-(n - i) dan disimpen di $t2
	
	#instruction to print $t1 + $t2
	li $v0,1
	add $a0,$t1,$t2		
	syscall
	
	addi $t4,$t4,4			#address $t4 +4
	addi $t8,$t8,-4			#address $t8 -4
	
	addi $t3,$t3,-1			#loop counter --
	
	j Penjumlahan
	
Output1:
	li $v0,4			#output jika user masukkan input tidak valid
	la $a0,output1		
	syscall			
	
Exit:
	li $v0,10			#exit command
	syscall
