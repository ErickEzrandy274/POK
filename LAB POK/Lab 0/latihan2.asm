.data 
myString: .asciiz "\nSaya berumur "
bulanLahir: .asciiz "\nBulan lahir: "
tahunnLahir: .asciiz "Tahun lahir: "
tahun: .asciiz " tahun."

.text 
.globl main
main:
	li $v0,4 		#instruction to print string bulanLahir
	la $a0,bulanLahir 	#move string bulanLahir to argumen
	syscall 		#execute
	
	li $v0,5 		#read integer from user
	syscall 
	
	add $t0,$v0,$zero 	#move integer bulanLahir to $t0
	
	li $v0,4 		#instruction to print string tahunLahir
	la $a0,tahunnLahir 	#move string tahunLahir to argumen
	syscall 		#execute
	
	li $v0,5 		#read integer from user
	syscall 
	
	add $t3,$v0,$zero 	#move integer tahunnLahir to $t3
	
	slti $t1, $t0,4		#if $t0 less than 4 than $t1 = 1, else $t1 = 0
	addi $t2,$zero,1	#penentuan masuk ke umurmhs atau umurmhs1
	
	beq $t1,$t2,umurmhs
	bne $t1,$t2,umurmhs1
	
umurmhs:
	addi $t2,$zero,2021
	sub $s0,$t2,$t3		#umurmhs adalah 2021 - tahunLahir
	
	li $v0,4
	la $a0,myString 	#print myString
	syscall 
	
	li $v0,1 		#instruction to print integer
	add $a0,$s0,$zero 	#move integer to argumen
	syscall 
	
	li $v0,4
	la $a0,tahun 		#print tahun
	syscall 
	
	li $v0,10		#exit command
	syscall
	
umurmhs1:
	#bagian ini jalan jika bulanLahir mhs lebih dari Maret
	addi $t2,$zero,2021
	sub $s0,$t2,$t3		#umurmhs adalah 2021 - tahunLahir
	subi $s0,$s0,1		#setelah itu umurnya -1 lagi karena belum ultah
	
	li $v0,4
	la $a0,myString 	#print myString
	syscall 
	
	li $v0,1 		#instruction to print integer
	add $a0,$s0,$zero 	#move integer to argumen
	syscall 
	
	li $v0,4
	la $a0,tahun 		#print tahun
	syscall 
	
	li $v0,10		#exit command
	syscall	
			
	
