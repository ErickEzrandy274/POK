.data
inputMessage1: .asciiz "Tanggal lahir: "
inputMessage2: .asciiz "Bulan lahir: "
inputMessage3: .asciiz "Tahun lahir: "
output1: .asciiz "\nSaya berumur "
output2: .asciiz " tahun."

.text
.globl main
main:
    
input:
    li $v0, 4 			#load service code to print string (service code 4)
    la $a0, inputMessage1 	#loads inputMessage1 to $a0
    syscall 			#execute
    
    li $v0, 5 			#load service code to read a string (service code 5)
    syscall 			#execute
    
    add $t5, $v0, $zero	#move first input (date) to $t5 (temp)
    
    li $v0, 4 			#load service code to print string (service code 4)
    la $a0, inputMessage2 	#loads inputMessage2 to $a0
    syscall 			#execute
    
    li $v0, 5 			#load service code to read a string (service code 5)
    syscall 			#execute
    
    add $t0, $v0, $zero	#move second input (month) to $t0 (temp)
        
    li $v0, 4 			#load service code to print string (service code 4)
    la $a0, inputMessage3 	#loads inputMessage3 to $a0
    syscall 			#execute
    
    li $v0, 5 			#load service code to read a string (service code 5)
    syscall 			#execute
    
    add $t1, $v0, $zero	#move third input (year) to $t1 (temp)

countAgeFromYear:

    addi $t3, $zero, 2021	#Load immediate 2021 to register $t3
    sub $t4, $t3, $t1		#$t4 = $t3 - $t1  OR  $t4 = 2021 - inputYearBorn	(2021 because MARCH 2021)
    				#Now $t4 is your age if counted only from year difference.
checkingTheMonth:
    
    addi $t2, $t0, -3		#$t2 = $t0 - 3  OR  $t2 = inputMonthBorn - 3      (3 because MARCH 2021) 
    
    beq $t2, $zero, checkDate	#if your monthBorn is March, then check your dateBorn
    bgtz $t2, minusOne		#If $t2 > 0, then go to minusOne. $t4 will be deducted by 1 because your birthday is not here yet. (Later than March)
    blez $t2, noMinusOne       #if $t2 < 0, then go to noMinusOne.

checkDate:
    
    addi $t6, $t5, -12		#$t6 = $t5 - 12  OR  $t6 = inputDateBorn - 12      (12 because 12 MARCH 2021) (if your birthday is on the 12th, then you are already a year older.)
    bgtz $t6, minusOne		#If $t6 is positive, then your birthday has not already occured, so minusOne.
    j noMinusOne		#if $t6 is zero or negative (it means that $t5 <= 12) then your birthday has already happened. So, no need to minusOne.
    				
noMinusOne:
    
    j PRINT	#If your birthday is before or in March, then your age is 2021 - inputYearBorn. So don't need to do anything to $t4 and just jump to Label PRINT

minusOne:

    addi $t4, $t4, -1		#Deducted $t4 by 1 because your birthday is later than March which is NOW
        
PRINT:  #print the output

    li $v0, 4 			#load service code to print string (service code 4)
    la $a0, output1	 	#loads output1 to $a0
    syscall 			#execute (print output1)

    li $v0, 1 			#load service code to print integer (service code 1)
    add $a0, $t4, $zero 	#move $t4 to $a0
    syscall 			#execute (print $t4)
    
    li $v0, 4 			#load service code to print string (service code 4)
    la $a0, output2	 	#loads output2 to $a0
    syscall 			#execute (print output2)
    
    li $v0, 10 # exit command
    syscall # execute
