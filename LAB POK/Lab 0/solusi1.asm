.data
inputMessage1: .asciiz "Jumlah Saudara: "
output1: .asciiz "\nHalo, nama saya Rico. Saya merupakan mahasiswa POK kelas A. Saya memiliki "
output2: .asciiz " saudara."

.text
.globl main
main:
    
    li $v0, 4 			#load service code to print string (service code 4)
    la $a0, inputMessage1 	#loads inputMessage1 to $a0
    syscall 			#execute
    
    li $v0, 5 			#load service code to read a string (service code 5)
    syscall 			#execute
    
    add $t0, $v0, $zero
    
    li $v0, 4 			#load service code to print string
    la $a0, output1	 	#loads output1 to $a0
    syscall 			#execute (print)
    
    li $v0, 1 			#load service code to print integer (service code 1)
    add $a0, $t0, $zero	#loads value of $t0 to $a0
    syscall 			#execute (print)
    
    li $v0, 4 			#load service code to print string
    la $a0, output2	 	#loads output2 to $a0
    syscall 			#execute (print)
    
    li $v0, 10 # exit command
    syscall # execute