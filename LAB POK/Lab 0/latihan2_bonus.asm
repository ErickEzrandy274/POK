.data 
tanggalLahir: .asciiz "Tanggal lahir: "
bulanLahir: .asciiz "Bulan lahir: "
tahunnLahir: .asciiz "Tahun lahir: "
myString: .asciiz "\nSaya berumur "
tahun: .asciiz " tahun."

.text 
.globl main
main:
	li $v0,4 		#instruction to print string tanggalLahir
	la $a0,tanggalLahir 	#move string tanggalahir to argumen
	syscall 		#execute
	
	li $v0,5 		#read integer from user
	syscall 
	
	add $t0,$v0,$zero 	#move integer tanggalLahir to $t0
	
	li $v0,4 		#instruction to print string bulanLahir
	la $a0,bulanLahir 	#move string bulanLahir to argumen
	syscall 		#execute
	
	li $v0,5 		#read integer from user
	syscall 
	
	add $t1,$v0,$zero 	#move integer bulanLahir to $t1
	
	li $v0,4 		#instruction to print string tahunLahir
	la $a0,tahunnLahir 	#move string tahunLahir to argumen
	syscall 		#execute
	
	li $v0,5 		#read integer from user
	syscall 
	
	add $t2,$v0,$zero 	#move integer tahunLahir to $t2
	
	slti $t3,$t1,4		#if $t1 less than 4 than $t3 = 1, else $t3 = 0
	slti $t4,$t0,13		#if $t1 less than 4 than $t4 = 1, else $t4 = 0
	addi $t5,$zero,1	#penentuan masuk ke umurmhs atau umurmhs1
	
	beq $t3,$t5,validate	#kalau bulannya less than April perlu cek lagi tanggalnya
	bne $t3,$t5,umurmhs1	#kalau bulannya greater than March berarti umurnya belum +1
	
validate:
	beq $t4,$t5,umurmhs
	bne $t4,$t5,umurmhs1
	
umurmhs:
	addi $s1,$zero,2021
	sub $s1,$s1,$t2		#umurmhs adalah 2021 - tahunLahir
	
	li $v0,4
	la $a0,myString 	#print myString
	syscall 
	
	li $v0,1 		#instruction to print integer
	add $a0,$s1,$zero 	#move integer to argumen
	syscall 
	
	li $v0,4
	la $a0,tahun 		#print tahun
	syscall 
	
	li $v0,10		#exit command
	syscall	
	
umurmhs1:
	addi $s1,$zero,2021
	sub $s1,$s1,$t2		#umurmhs adalah 2021 - tahunLahir
	subi $s1,$s1,1		#setelah itu umurnya -1 lagi karena belum ultah
	
	li $v0,4
	la $a0,myString 	#print myString
	syscall 
	
	li $v0,1 		#instruction to print integer
	add $a0,$s1,$zero 	#move integer to argumen
	syscall 
	
	li $v0,4
	la $a0,tahun 		#print tahun
	syscall 
	
	li $v0,10		#exit command
	syscall	
	
	