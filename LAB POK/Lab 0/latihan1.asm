.data 
myString: .asciiz "\nHalo, nama saya Rico. Saya merupakan mahasisawa POK kelas A. Saya memiliki "
input: .asciiz "Jumlah Saudara: "
myString1: .asciiz " saudara"

.text 
.globl main
main:
	li $v0,4 		#instruction to print string
	la $a0,input 		#move string to argumen
	syscall 		#execute
	
	li $v0,5 		#read integer from user
	syscall 
	
	add $t0,$v0,$zero 	#move integer to $t0
	
	li $v0,4
	la $a0,myString 	#print myString
	syscall 
	
	li $v0,1 		#instruction to print integer
	add $a0,$t0,$zero 	#move integer to argumen
	syscall 
	
	li $v0,4
	la $a0,myString1 	#print myString1
	syscall 
	
	li $v0,10		#exit command
	syscall 