.data
array: .byte 30, 12, 41, 13, 15 
dataUrut: .asciiz "Data yang sudah diurutkan: "
konjungsi: .asciiz ", "
array_end: .byte 13 #hanya sekadar ingin tau length array berapa jadi isinya asal
str_median: .asciiz "Mediannya adalah: "
str_modus: .asciiz "Modusnya adalah: "
# Hint : Counter digunakan untuk menyimpan hasil perhitungan array (see counting sort)
counter: .space 256

.text
.globl main
main:
	#itung alamat array
	li $v0,1
	la $a0,array
	move $t0,$a0
	
	#itung alamat array_end
	li $v0,1
	la $a0,array_end
	move $t1,$a0
	
	sub $t1,$t1,$t0			#lengthnya array
	
	addi $t2,$zero,1		#i = 1

OuterLoop:
	beq $t2,$t1, HasilSort
	
	#Hold the value of a[i]
	move $t3,$t2			#t3 = i
	add $t3,$t3,$t0
	lbu $t3,0($t3)			#value of $t3 = a[i]		
	
	#for(j = i-1; j>=0 && a[j] > value; j--)
	addi $t4,$t2,-1			#j = t4
	
InnerLoop:
	slt $t5,$t4,$zero		#t5 = 1 if j<0
	bne $t5,$zero,GoOuter
	
	#a[j] > value
	move $t6, $t4
	add $t6, $t6, $t0
	lbu $t6, 0($t6)			
	slt $t7,$t6, $t3		#t7 = 1 if t6<value
	bne $t7, $zero, GoOuter
	
	#a[j+1] = a[j]
	addi $t8,$t4,1
	move $t9, $t8
	add $t9, $t9, $t0
	sb $t6, 0($t9)
	addi $t4, $t4, -1
	j InnerLoop
	
GoOuter:
	#a[j+1] = value
	addi $s0,$t4, 1
	move $s1, $s0
	add $s1, $s1, $t0
	sb $t3, 0($s1)
	addi $t2, $t2, 1
	j OuterLoop

HasilSort:
	li $v0,4
	la $a0,dataUrut
	syscall
	addi $t1, $t1, -2
	move $t3,$zero
	
CetakHasil:
	beq $t3, $t1, CetakElemenAkhir
	add $t3, $t3, $t0
	lbu $t4, 0($t3)
	
	li $v0,1
	add $a0,$zero,$t4		
	syscall
	
	li $v0,4
	la $a0,konjungsi		
	syscall
	
	addi $t3, $t3, 1
	j CetakHasil

CetakElemenAkhir:
	addi $t1, $t1, 1
	add $t1, $t0, $t1
	lbu $t4, 0($t1)
	
	li $v0,1
	add $a0,$zero,$t4		
	syscall

HitungMedian:
	#cari median jika lengthny ganjil = (length+1)/2
	addi $t1, $t1, -1		#panjang di -1
	addi $s2, $zero, 2
	div $t1,$s2
	mflo $s3			#nyimpen hasil bagi
	add $s3,$s3,$t0
	lbu $s4, 0($s3)			#isinya nilai tengah
	
	li $v0,4
	la $a0,str_median		
	syscall
	
	li $v0,1
	add $a0,$zero,$s4		
	syscall

hitung_kemunculan:

hitung_modus:

exit:
	li $v0,10			#exit command
	syscall