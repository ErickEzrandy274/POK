.data
array: .byte 21, 42, 56, 57, 13, 12, 13, 41, 15, 12, 57	# array yang akan diubah oleh asdos untuk penilaian.
array_end:	# pembatas dari akhir array
		# dapat digunakan untuk menghitung panjang array

str_data_terurut: .asciiz "Data yang sudah diurutkan: "
str_median: .asciiz "\nMediannya adalah: "
str_modus: .asciiz "\nModusnya adalah: "
str_cari_data: .asciiz "\nData ke-berapa yang anda inginkan? "
str_result: .asciiz "Data tersebut adalah "

str_kasihjarak: .asciiz ", "

counter: .space 256	# counter untuk menyimpan kemunculan angka
                         
.text
.globl main
main:
        li, $t0, 0
        
set_counter:			# nilai di counter diatur menjadi nol semua
	sb $0, counter($t0)
	add $t0, $t0, 1
	blt $t0, 256, set_counter

la $t3, array_end
la $t4, array
sub $t3, $t3, $t4		# register $t3 menyimpan banyaknya data yang akan dikalkulasi
add $t5, $t3, $zero
        
li $t0, 0			# indeks/pointer ke berapa data yang telah dikalkulasi
count_occurence:
	lbu $t1, array($t0)	# dilakukan load byte ke $t1 untuk setiap data angka yang tersimpan di array
	
count:
	lb $t2, counter($t1)	# ambil nilai yang tersimpan pada counter[$t1] siimpan ke $t2 untuk mengetahui nilai awalnya
	add $t2, $t2, 1		# update nilai $t2 dengan menambahkan satu
	sb $t2, counter($t1)	# simpan kembali nilai $t2 setelah update pada counter[$t1]
	add $t0, $t0, 1		# pointer ditambah satu
	blt $t0, $t3, count_occurence	# jika pointer kurang dari banyaknya data maka lakukan lb dan kalkulasi lagi 
	
        
li $t0, 0
li $t1, 0
add $t2, $t3, $zero		# t2 menyimpan jumlah data
	
li $v0, 4
la $a0, str_data_terurut	# cetak string
syscall
	
cetak_data:				# cetak data yang sudah diurutkan
	lb $t1, counter($t0)
	beq $t1, 0, pass
	
cetak_angka:
	li $v0, 1
	move $a0, $t0			# cetak angka
	syscall
	sub $t2, $t2, 1
	beq $t2, 0, next_step		# jika semua data telah dicetak maka langsung ke next_step
	li $v0, 4
        la $a0, str_kasihjarak
	syscall
	
	sub $t1, $t1, 1
	bne $t1, 0, cetak_angka		# cetak sesuai jumlah kemunculannya
pass:
	add $t0, $t0, 1
	blt $t0, 256, cetak_data

# t5 = len array 
# hitung median = len(array sorted)/2
# t0 = index/pointer
# t1 = count
# t2 = total
# t3 = floor(len/2)
next_step:
li $t0, 0
li $t1, 0
li $t2, 0
div $t3, $t5, 2
        
hitung_median_atau_data:		# baik median ataupun data ke-n menggunakan prinsip pencarian yang sama
	lb $t1, counter($t0)
	add $t2, $t2, $t1
	bgt $t2, $t3, hitung_median_atau_data_complete	# total count > floor(len/2), median telah ditemukan yaitu nilai pada $t0
        add $t0, $t0, 1			# pointer +1
        j hitung_median_atau_data			# lakukan lb lagi hingga total count > floor(len/2)

hitung_median_atau_data_complete:
	beq $t8, 1, end			# t8 adalah flag jika nilainya 1 maka yang akan dicetak adalah data ke-n
	li $v0, 4
	la $a0, str_median
	syscall
            
	li $v0, 1
	move $a0, $t0			# cetak median
	syscall
            
            
li $t0, 0	# index/pointer
li $t1, 0	# count
li $t2, 0	# max count/ jumlah kemunculan terbanyak
li $t6, 0

hitung_modus_occurence:
	lb $t1, counter($t0)
	blt $t1, $t2, skip
	add $t2, $t1, $zero
	add $t6, $t0, $zero	# t6 menyimpan nilai modus terbesar
skip:
	beq $t0, 255, hitung_modus_complete 
	add $t0, $t0, 1
	j hitung_modus_occurence

hitung_modus_complete:
	li $v0, 4
	la $a0, str_modus
	syscall

li $t0, 0
li $t1, 0
tampilkan_modus:
	lb $t1, counter($t0)
	bne $t1, $t2, next
	
	li $v0, 1
	move $a0, $t0
	syscall
	
	beq $t0, $t6, cari_data
	li $v0, 4
        la $a0, str_kasihjarak
	syscall
next:
	beq $t0, $t6, cari_data
	add $t0, $t0,1
	j tampilkan_modus
cari_data:
li $t0, 0
li $t1, 0
li $t2, 0
li $t8, 1

li $v0, 4
la $a0, str_cari_data
syscall
li $v0, 5 			
syscall

move $t3, $v0
sub $t3, $t3, 1			# t3 tidak harus dikurangi 1, hanya menyesuaikan dengan kode yang sudah ada 
j hitung_median_atau_data	# pada blok hitung_median_atau_data

end:
li $v0, 4
la $a0, str_result
syscall

li $v0, 1
move $a0, $t0			# cetak data
syscall

li $v0, 10
syscall
		
