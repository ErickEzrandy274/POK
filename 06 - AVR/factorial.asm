.include "m8515def.inc"
.def temp = r18
.def tempin = r19 ; Define temporary variable
.def tempout = r20

start:
    ldi temp,low(RAMEND)
    out SPL,temp
    ldi temp,high(RAMEND)
    out SPH,temp
    ldi r16, 3
    mov tempin, r16
    rcall fact

fact:
    push temp
    push tempin

basecase:
    tst tempin
    breq endcase

reccase:
    mov temp, tempin
    dec tempin
    rcall fact
    mul temp, tempout
    mov tempout, r0
    rjmp outrec

endcase:
    ldi tempout, 1

outrec:
    pop tempin
    pop temp
    ret

forever:    
    rjmp forever
