15s = 1 x 200.000 FP + 800.000 NFP (J1 & Pr1)
35s = 3 x 200.000 FP + 800.000 NFP (J2 & Pr1) 
_____________________________________________ -
20s = 2 x 200.000 FP
10s = 200.000 FP (1 instruksi FP memerlukan 10 detik)

waktu J2:
35s = 3 x 200.000 FP + NFP
35s = 30s + 5s
(waktu FP = 30s dan waktu NFP = 5s)

J2 dengan Pr2
20s = 3 x 200.000 FP + NFP
20s = 3T + 5
5s  = T (1 instruksi FP memerlukan 5 detik dimana Pr2 mendapat speedup 2x pada FP)

J1 dengan Pr2
Time = FP + NFP
     = 5s + 5s
     = 10s 